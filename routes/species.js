var fetch = require('node-fetch');
var env = require('../env');
var common = require('./common');

module.exports = (router) => {

  router.get('/', function (req, res) {

    var urls = [
        env.speciesurl + env.urlsuffix
    ];

    common.getParallel(res, urls);

  });

  router.get('/:id', function (req, res) {

    var id = req.params.id;
    var url = env.speciesurl + id + "/" + env.urlsuffix;

    // url example: https://swapi.co/api/species/2/?format=json
    fetch(url)
      .then(function(res) {
          return res.json();
      }).then(function(json) {
          console.log(json);
          res.send(json);
      });
  });

  router.post('/', function (req, res) {
    res.send('Post planets!');
  });

  return router;
}
