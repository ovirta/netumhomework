var request = require('request');

module.exports = {

  getParallel: async function(res, urls) {
      var data;
      var jsonarray = [];
      var next = true;

      while(next)
      {
        try {
            data = await Promise.all(urls.map(requestAsync));
        } catch (err) {
            console.error(err);
        }
        var jsondata = JSON.parse(data);

        urls  = [
          jsondata['next']
        ];

        var planetdata = jsondata['results'];

        if(planetdata) {
          jsonarray = jsonarray.concat(planetdata);
        }
        if(urls[0]) {
            console.log(urls[0]);
            next = true;
          }
          else {
            next = false;
          }
      }
      res.send (jsonarray);
  }
}

  var requestAsync = function(url) {
      return new Promise((resolve, reject) => {
          var req = request(url, (err, response, body) => {
              if (err)
                return reject(err, response, body);
              resolve(body);
          });
      });
  }
