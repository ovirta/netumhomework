var fetch = require('node-fetch');
var env = require('../env');
var common = require('./common');

const urlsuffix = '?format=json';

module.exports = (router) => {

  // url example: https://swapi.co/api/planets/?format=json
  router.get('/', function (req, res) {

    var urls = [
        env.planetsurl + env.urlsuffix
    ];

    common.getParallel(res, urls);

  });

  router.get('/:id', function (req, res) {

    var id = req.params.id;
    var url = env.planetsurl + id + "/" + env.urlsuffix;

    // url example: https://swapi.co/api/planets/2/?format=json
    fetch(url)
      .then(function(res) {
          return res.json();
      }).then(function(json) {
          console.log(json);
          res.send(json);
      });
  });


  router.post('/', function (req, res) {
    res.send('Post planets!');
  });

  return router;
}
