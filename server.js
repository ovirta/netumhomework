// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var fetch      = require('node-fetch');
var env        = require('./env');
var cors       = require('cors');
const path     = require('path');


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// Enable ng serve development
app.use(cors({ origin: 'http://localhost:4200' }));

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router
var router2 = express.Router({mergeParams: true}); // second router conf

var planets = require('./routes/planets')(router); // Import Planet Routes
app.use('/api/planets', planets); // Use routes in application

var species = require('./routes/species')(router2); // Import Species Routes
app.use('/api/species', species); // Use routes in application

// test route to make sure everything is working (accessed at GET http://localhost:8080/)
/*router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});*/

app.use(express.static(__dirname + '/dist/'));

app.get('*', (req, res) => {
    //res.send('<h1>hello world db</h1>');
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});


// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
