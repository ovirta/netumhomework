## NetumHomework

Repository contains **Node, Express, Angular 2** SWAPI planet browser. **Bootstrap** library is used  for rendering UI.

*Angular 2 frontend* is requesting data over *Node-Express backend* via REST API.

Planet data is fetched with several requests. While backend is fetching data, frontend is showing a *loading spinner*. Individual planet and species data is fetched with node-fetch library.

Consecutive data fetching is not optimized so that on each planet list page refresh the data is fetched again from the server. Sorting of the planets is implemented on the client side. Planets that have diameter 0 (zero) or unknown are listed in arbitrary order.

## Installation

```
git clone git@bitbucket.org:ovirta/netumhomework.git
cd netumhomework
npm install
node server.js
```

After node server is running, open [http://localhost:8080](http://localhost:8080) with your favourite browser.

## API listing

Only HTTP Get methods are implemented.

### Get
```
/api/planets
/api/planets/:id
/api/species
/api/species/:id
```
