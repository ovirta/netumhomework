import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Species } from '../species';
import { SpeciesService }  from '../services/species.service';
import { MessageService }  from '../services/message.service';

@Component({
  selector: 'app-species-detail',
  templateUrl: './species-detail.component.html',
  styleUrls: ['./species-detail.component.css']
})
export class SpeciesDetailComponent implements OnInit {

  @Input() species: Species;
  loaded: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private speciesService: SpeciesService,
    private location: Location,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getSpecies();
  }

  getSpecies(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.messageService.add(`SpeciesDetail: fetch species id=${id}`);
    this.loaded = false;
    this.speciesService.getSingleSpecies(id)
      .subscribe(species => {
        this.species = species;
        this.loaded = true;
      });
  }

  goBack(): void {
    this.location.back();
  }

}
