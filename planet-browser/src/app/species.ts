export class Species {
  name: string;
  homeworld: string;
  language: string;
  url: string;
  id: number;
  people: string[];
}
