import { Component, OnInit } from '@angular/core';
import { Planet } from '../planet';
import { Species } from '../species';
import { PlanetService } from './../services/planet.service';
import { SpeciesService } from './../services/species.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent implements OnInit {

  planets: Planet[];
  species: Species[];
  sortDirection: Boolean = true;
  loaded: Boolean = false;

  constructor(
    private planetService: PlanetService,
    private speciesService: SpeciesService,
  ) {
    this.sortDirection = false;
    this.loaded = false;
  }

  ngOnInit() {
    this.getPlanets();
    this.getSpecies();
  }

  getSpecies() {
    this.speciesService.getSpecies()
      .subscribe(species => this.species = species);
  }

  getPlanets() {
    this.loaded = false;
    this.planetService.getPlanets()
        .subscribe(planets => {
          this.planets = planets;
          this.loaded = true;
        });
  }

  toggleSortDirection() {
    this.sortDirection = !this.sortDirection;
  }

  getPlanetsSortedByDiameter() : Planet[] {
    if(this.planets)
      if(this.sortDirection == true)
        return this.planets.sort(function(a, b) {
          var first = a['diameter'];
          var second = b['diameter'];

          if(""+first == "unknown" && ""+second == "unknown" || first == 0 && second == 0)
            return a['id'] - b['id'];
          else if(""+first == "unknown")
            first = -1;
          else if(""+second == "unknown")
            second = -1;

          return +first - +second;
        });
      else
        return this.planets.sort(function(a, b) {
          var first = a['diameter'];
          var second = b['diameter'];

          if(""+first == "unknown" && ""+second == "unknown" || first == 0 && second == 0)
            return b['id'] - a['id'];
          else if(""+first == "unknown")
            first = -1;
          else if(""+second == "unknown")
            second = -1;

          return +second - +first;
      });
  }

  getSpeciesOnPlanet(url: string): Species[] {
    if(!this.planets || !this.species) return;

      return this.species.filter(s => s['homeworld'] == url);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.planetService.addPlanet({ name } as Planet)
      .subscribe(planet => {
        this.planets.push(planet);
      });
    }

    delete(planet: Planet): void {
      this.planets = this.planets.filter(h => h !== planet);
      this.planetService.deletePlanet(planet).subscribe();
    }

    getId(url: string): number {
      var urlitems = url.split('/');
      var idIndex = 5;
      return +urlitems[idIndex];
    }

    getPlanetSpecies(url: string): number[] {
      let list: number[] = [1, 2, 3];
      return list;
    }
}
