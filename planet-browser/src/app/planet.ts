export class Planet {
  name: string;
  diameter: number;
  id: number;
  url: string;
}
