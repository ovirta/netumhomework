import { Component, OnInit } from '@angular/core';
import { Species } from '../species';
import { SpeciesService } from './../services/species.service';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit {

  species: Species[];

  constructor(
    private speciesService: SpeciesService,
  ) { }

  ngOnInit() {
    this.getSpecies();
  }

  getSpecies(): void {
    this.speciesService.getSpecies()
      .subscribe(species => this.species = species);
  }

  getId(url: string): number {
    var urlitems = url.split('/');
    var idIndex = 5;
    return +urlitems[idIndex];
  }

  getPlanetSpecies(url: string): Species[] {
    if(this.species) {
      var planetspecies = this.species.filter(spec => spec['homeworld'] == url);
      return planetspecies;
    }
  }
}
