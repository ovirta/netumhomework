import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Species } from './../species';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class SpeciesService {

  private speciesUrl = 'http://localhost:8080/api/species';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
  ) { }

    /** Log a speciesService message with the MessageService */
    private log(message: string) {
      this.messageService.add('SpeciesService: ' + message);
    }

    /** GET planets from the server */
    getSpecies(): Observable<Species[]> {
      return this.http.get<Species[]>(this.speciesUrl)
        .pipe(
          tap(species => this.log(`fetched species`)),
          catchError(this.handleError('getSpecies', []))
        );
    }

    getSingleSpecies(id: number): Observable<Species> {
      const url = `${this.speciesUrl}/${id}`;
      return this.http.get<Species>(url).pipe(
        tap(_ => this.log(`fetched species id=${id}`)),
        catchError(this.handleError<Species>(`getSingleSpecies id=${id}`))
      );
    }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
