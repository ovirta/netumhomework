import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Planet } from './../planet';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PlanetService {

  private planetsUrl = 'http://localhost:8080/api/planets';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

    /** Log a planetService message with the MessageService */
    private log(message: string) {
      this.messageService.add('PlanetService: ' + message);
    }

    /** GET planets from the server */
    getPlanets(): Observable<Planet[]> {
      return this.http.get<Planet[]>(this.planetsUrl)
        .pipe(
          tap(planets => this.log(`fetched planets`)),
          catchError(this.handleError('getPlanets', []))
        );
    }

    /** PUT: update the planet on the server */
    updatePlanet (planet: Planet): Observable<any> {
      return this.http.put(this.planetsUrl, planet, httpOptions).pipe(
        tap(_ => this.log(`updated planet id=${planet.id}`)),
        catchError(this.handleError<any>('updatePlanet'))
      );
    }

    /** POST: add a new planet to the server */
    addPlanet (planet: Planet): Observable<Planet> {
      return this.http.post<Planet>(this.planetsUrl, planet, httpOptions).pipe(
        tap((planet: Planet) => this.log(`added planet w/ id=${planet.id}`)),
        catchError(this.handleError<Planet>('addPlanet'))
      );
    }

    /** DELETE: delete the planet from the server */
    deletePlanet (planet: Planet | number): Observable<Planet> {
      const id = typeof planet === 'number' ? planet : planet.id;
      const url = `${this.planetsUrl}/${id}`;

      return this.http.delete<Planet>(url, httpOptions).pipe(
        tap(_ => this.log(`deleted planet id=${id}`)),
        catchError(this.handleError<Planet>('deletePlanet'))
      );
    }

    /* GET planets whose name contains search term */
    searchPlanets(term: string): Observable<Planet[]> {
      if (!term.trim()) {
        // if not search term, return empty planet array.
        return of([]);
      }
      return this.http.get<Planet[]>(`api/planets/?name=${term}`).pipe(
        tap(_ => this.log(`searched planets matching "${term}"`)),
        catchError(this.handleError<Planet[]>('searchPlanets', []))
      );
    }

    getPlanet(id: number): Observable<Planet> {
      const url = `${this.planetsUrl}/${id}`;
      return this.http.get<Planet>(url).pipe(
        tap(_ => this.log(`fetched planet id=${id}`)),
        catchError(this.handleError<Planet>(`getPlanet id=${id}`))
      );
    }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
