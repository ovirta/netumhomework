import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { PlanetsComponent }      from './planets/planets.component';
import { SpeciesComponent }      from './species/species.component';
import { DashboardComponent }    from './dashboard/dashboard.component';
import { PlanetDetailComponent } from './planet-detail/planet-detail.component';
import { SpeciesDetailComponent } from './species-detail/species-detail.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'planets', component: PlanetsComponent },
  { path: 'planets/:id', component: PlanetDetailComponent },
  { path: 'species', component: SpeciesComponent },
  { path: 'species/:id', component: SpeciesDetailComponent },
  { path: '', redirectTo: '/planets', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
