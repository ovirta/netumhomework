import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Planet } from '../planet';
import { PlanetService }  from '../services/planet.service';
import { MessageService }  from '../services/message.service';


@Component({
  selector: 'app-planet-detail',
  templateUrl: './planet-detail.component.html',
  styleUrls: ['./planet-detail.component.css']
})
export class PlanetDetailComponent implements OnInit {

  @Input() planet: Planet;

  constructor(
    private route: ActivatedRoute,
    private planetService: PlanetService,
    private location: Location,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getPlanet();
  }

  getPlanet(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.messageService.add(`PlanetDetail: fetch planet id=${id}`);

    this.planetService.getPlanet(id)
      .subscribe(planet => this.planet = planet);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.planetService.updatePlanet(this.planet)
      .subscribe(() => this.goBack());
 }
}
